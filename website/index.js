// Based on: https://www.w3schools.com/howto/howto_js_todolist.asp

var socket;
const backEndPort = 8080;
const groceryLists = new Map(); // DCT mapping the name of grocery lists to the actual list
var connected = false;
var createList = () => { toastr.warning("Cannot do that without connection to the back-end."); };

$( document ).bind( "mobileinit", function(){
    $.mobile.collapsible.prototype.options.collapsedIcon = "arrow-r";
});

// On page load
onload = function() {
    // Setup communication with the back-end
    if ("WebSocket" in window) {
        socket = new WebSocket("ws://127.0.0.1:" + backEndPort);

        socket.onopen = function() {
            connected = true;
            createList = makeList;

            flushBuffer();
            toastr.success('Connected with the back-end.');
        };

        socket.onmessage = onMessage;

        socket.onclose = function() {
            connected = false;
            createList = () => { toastr.warning("Cannot do that without connection to the back-end."); };
            toastr.warning('Lost connection with the back-end.');
        };
    }
    else {
        toastr.error('Your browser does not support WebSocket!');
    }
};

function onMessage(msg) {
    msg = JSON.parse(msg.data);
    // 'listUpdate', { name: name + " by " + author, list: newList.get() }
    if (msg.type === "listUpdate") {
        // Found a new or updated an existing grocery list service
        const name = msg.content.name;
        const list = msg.content.list;
        groceryLists.set(name, list);

        // Update the UI
        addGroceryList(name);
    }
    else if (msg.type === "buyResponse") {
        const status = msg.content.status;
        console.log("answer:");
        console.log(msg);
        if (status === 'ACCEPTED')
            toastr.success(`Buy request accepted: ${msg.content.description}.`);
        else
            toastr.warning(`Buy request rejected: ${msg.content.error}`);
    }
    else {
        toastr.warning('Did not understand message from the back-end.');
    }
}

/*
// Create a "close" button and append it to each list item
var myNodelist = document.getElementsByTagName("LI");
var i;
for (i = 0; i < myNodelist.length; i++) {
    var span = document.createElement("SPAN");
    var txt = document.createTextNode("\u00D7");
    span.className = "close";
    span.appendChild(txt);
    myNodelist[i].appendChild(span);
}

// Click on a close button to hide the current list item
var close = document.getElementsByClassName("close");
var i;
for (i = 0; i < close.length; i++) {
    close[i].onclick = function() {
        var div = this.parentElement;
        div.style.display = "none";
    }
}
*/

/*
// Add a "checked" symbol when clicking on a list item
var list = document.querySelector('ul');
list.addEventListener('click', function(ev) {
    if (ev.target.tagName === 'LI') {
        ev.target.classList.toggle('checked');
    }
}, false);
*/

// Create a new list item when clicking on the "Create" button
// The combination of `name` and `author` is expected to be unique.
function makeList() {
    var name = document.getElementById("nameInput").value;
    var author = document.getElementById("authorInput").value;

    document.getElementById("nameInput").value = "";
    document.getElementById("authorInput").value = "";

    addGroceryList(`${name} by ${author}`);

    // Inform the back end
    const msg = message("createGroceryService", {name: name, author: author});
    sendToBackEnd(msg);
}

// Adds a new grocery list to the UI
function addGroceryList(name) {
    if (name === '')
        toastr.error("Service name must not be empty.");

    if (!groceryLists.has(name)) {
        // Store the list
        groceryLists.set(name, []);
    }
    else {
        if (document.getElementById(name))
            document.getElementById(name).remove();
    }

    // Update the list
    const list = groceryLists.get(name);

    // Create a new list entry
    var div = `<div id="${name}" data-role="collapsible" data-collapsed-icon="carat-u" data-expanded-icon="carat-d">`;
    div += `<h4><p class="inline">${name}</p><a class="inline coll_head_butt" data-theme="b" onclick="showAddItem('${name}');" data-iconpos="notext" data-role="button" data-icon="plus"></a></h4>`;
    div += `<ul data-role="listview">`;

    list.forEach(item => {
        // name, requested, bought
        div += `<li><a href="#">${item.name} (${item.bought}/${item.requested})</a>`;
        div += `<a class="inline coll_head_butt" data-theme="b" onclick="buyItem('${name}', '${item.name}', ${item.requested}, ${item.bought});" data-iconpos="notext" data-role="button" data-icon="shop"></a></li>`;
    });

    div += `</ul></div>`;

    document.getElementById("main").innerHTML += div; //appendChild(htmlToElement(div));
    //$(div).appendTo('#main').page(); //trigger("create");
    $('div[data-role=collapsible]').collapsible(); // Tell jquery to update collapsibles in the UI
    $('#main').trigger("create");

    /*
    var li = document.createElement("li");
    var t = document.createTextNode(name);
    li.appendChild(t);
    */

    /*
    else
        document.getElementById("listUL").appendChild(li);

    var span = document.createElement("SPAN");
    var txt = document.createTextNode("\u00D7");

    span.className = "close";
    span.appendChild(txt);
    li.appendChild(span);
    */

    ////////
    //const div = document.createElement("div");


    /*
    <div data-role="collapsible">
        <h4>C</h4>
        <ul data-role="listview">
            <li><a href="#">Calvin</a></li>
            <li><a href="#">Cameron</a></li>
            <li><a href="#">Chloe</a></li>
            <li><a href="#">Christina</a></li>
        </ul>
    </div>
     */

    /*
    for (i = 0; i < close.length; i++) {
        close[i].onclick = function() {
            var div = this.parentElement;
            div.style.display = "none";
        }
    } */
}

// Shows a dialog to add a grocery item to the given list
function showAddItem(listName) {
    var div = `<div id="dialog" title="Add Grocery Item to '${listName}'">`;
    div += `<input type="text" id="groceryName" placeholder="Item..." />`;
    div += `<input type="number" id="groceryQty" min="1" />`;
    div += `<button onclick="addItem('${listName}')" class="ui-button ui-widget ui-corner-all">Add</button>`;
    div += `</div>`;

    document.body.innerHTML += div;
    $('#dialog').dialog({
        defaults: true
    });
}

// Sends a message to the back-end to add the item
function addItem(listName) {
    const name = document.getElementById('groceryName').value;
    const qty = parseInt(document.getElementById('groceryQty').value);

    console.log('Adding: ' + qty + ' x ' + name);

    // Send information to back-end, which will update the grocery list
    // and as such will trigger the onUpdate and update the UI.
    // Inform the back end
    const msg = message("addItem", {list: listName, name: name, quantity: qty});
    sendToBackEnd(msg);

    $('#dialog').dialog("close");
    document.getElementById('dialog').remove();
}

// Shows a dialog to buy a certain quantity of the item
function buyItem(listName, itemName, requested, bought) {
    var div = `<div id="buyDialog" title="Buy '${itemName}' (${bought}/${requested})">`;
    div += `<label for="buyQty">Quantity:</label>`;
    div += `<input type="number" id="buyQty" min="1" />`;
    div += `<button onclick="proceedWithBuy('${listName}', '${itemName}', ${requested}, ${bought})" class="ui-button ui-widget ui-corner-all">Send buy request</button>`;
    div += `</div>`;

    document.body.innerHTML += div;
    $('#buyDialog').dialog({
        defaults: true
    });
}

function proceedWithBuy(listName, itemName, requested, bought) {
    const buyQty = parseInt(document.getElementById('buyQty').value);
    const remaining = requested - bought;

    if (buyQty > remaining) {
        toastr.warning(`Cannot buy ${buyQty} pieces, only ${remaining} pieces remaining of ${requested} requested pieces.`);
    }
    else {
        // Send buy request to the back-end
        const msg = message("buyItem", {list: listName, item: itemName, quantity: buyQty});
        sendToBackEnd(msg);

        $('#buyDialog').dialog("close");
        document.getElementById('buyDialog').remove();

        toastr.info(`Sent buy request for: ${buyQty} x ${itemName}`);
    }
}

// Updates an existing grocery list
function updateGroceryList() {
    // TODO
}


// ------ NETWORKING FUNCTIONS ------
function message(type, content) {
    return {type: type, content: content};
}

var buffer = [];
function sendToBackEnd(msg) {
    if (connected)
        socket.send(JSON.stringify(msg));
    else
        buffer.push(msg);
}

function flushBuffer() {
    buffer.forEach(sendToBackEnd);
    buffer = [];
}

// ------ TOASTR CONFIGURATION ------
toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": true,
    "progressBar": false,
    "positionClass": "toast-bottom-center",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "3000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};


Map.prototype.getOrElse = function(key, notSetValue) {
    return this.has(key) ? this.get(key) : notSetValue;
};

// Taken from:
// https://stackoverflow.com/questions/494143/creating-a-new-dom-element-from-an-html-string-using-built-in-dom-methods-or-pro
function htmlToElement(html) {
    var template = document.createElement('template');
    html = html.trim(); // Never return a text node of whitespace as the result
    template.innerHTML = html;
    return template.content.firstChild;
}