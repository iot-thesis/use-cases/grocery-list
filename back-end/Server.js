const GroceryItem = require('./Grocery');
const {createGroceryService, sub} = require('./GroceryService');
const groceryServices = new Map(); // Maps names to services

/*
 * Initialize websocket
 */

var WebSocketServer = require('ws').Server,
    port = 8080,
    wss = new WebSocketServer({port: port});

console.log('Websocket server listening at http://127.0.0.1:' + port);

/*
 * Listen for connections and messages
 */

var socket;
var buffer = [];
wss.on('connection', function(sock) {
    socket = sock;
    onSocket(sock);
    socket.on('message', function(msg) {
        msg = JSON.parse(msg);
        if (!msg.type || !msg.content)
            console.log('Did not understand message coming from the front-end.');
        else
            onMessage(msg);
    });

    socket.on('close', function() {
        socket = null;
    });

    socket.on('error', function(error) {
        console.log('Socket error: ' + error);
        socket = null;
    });
});

function onSocket(s) {
    socket = s;
    flushBuffer();
}

function flushBuffer() {
    buffer.forEach(sendMessage);
    buffer = [];
}

// Pushes the new/updated grocery list to the front-end
function pushUpdate(name, author, newList) {
    var msg = message('listUpdate', { name: name + " by " + author, list: newList.get() });
    sendMessage(msg);
}

function message(type, content) {
    return {type: type, content: content};
}

// Subscribe to grocery services
sub(service => {
    service.groceryList.onUpdate(pushUpdate.bind(null, service.name, service.author));
    pushUpdate(service.name, service.author, service.groceryList);
    groceryServices.set(service.name + " by " + service.author, service);
});

/*
 * Processes received messages
 */

function onMessage({type, content}) {
    switch (type) {
        case 'createGroceryService':
            var s = createGroceryService(content.name, content.author, pushUpdate.bind(null, content.name, content.author));
            groceryServices.set(s.name + " by " + s.author, s);
            break;
        case 'addItem':
            if (!groceryServices.has(content.list))
                throw new Error('Unknown grocery list: ' + content.list);
            var s = groceryServices.get(content.list);
            s.groceryList.add(new GroceryItem(content.name, content.quantity));
            break;
        case 'buyItem':
            if (!groceryServices.has(content.list))
                throw new Error('Unknown grocery list: ' + content.list);
            var s = groceryServices.get(content.list);

            s.buy(content.item, content.quantity)
             .then(() => {
                // Inform front-end of success
                const msg = message('buyResponse', {status: 'ACCEPTED', description: content.quantity.toString() + ' x ' + content.item});
                sendMessage(msg);
             })
             .catch(error => {
                // Inform front-end of failure
                const msg = message('buyResponse', {status: 'REJECTED', error: error});
                sendMessage(msg);
             });
            break;
        default:
            console.log('Unknown message type \'' + type + '\'');
    }
}

function sendMessage(msg) {
    if (socket)
        socket.send(JSON.stringify(msg));
    else
        buffer.push(msg);
}

module.exports.pushUpdate = pushUpdate;