const GroceryItem = require('./Grocery');

/*
 *   GroceryList contains GroceryItems  (GroceryList is a Secro)
 *      - add(item: GroceryItem)     --> if map.has(item.name) then add quantities else new entry
 *      - [ buy(itemName)            --> Buys the item (willen we niet laten propageren) ]
 *         Implement buy as a method of the Service --> because it coordinates between both, the inventory and the list
 *      - bought(itemName, quantity) --> Updates the quantity of that item in the list: item.quantity -= quantityBought
 *      - get(): GroceryItem[]       --> Returns an array of grocery items
 *
 *   Inventory of bought GroceryItems (maps GroceryItems to amount bought)   (Strongly consistent replica)
 *      - amountInStock(itemName: string): number --> returned hoeveel van dat item men reeds gekocht heeft
 *      - approve(itemName: string, stockQuantity: number, buyingQuantity: number): boolean
 *          --> The client asks the server if he can buy a `buyingQuantity` quantity of `itemName` items.
 *              The client is aware/thinks there are `stockQuantity` of this item in stock
 *              (i.e. is the amount the server responded with when asking `amountInStock`).
 *              If the server approves this means he trusts you will buy that quantity and he adds the quantity to the inventory.
 */

Map.prototype.getOrElse = function(key, notSetValue) {
    return this.has(key) ? this.get(key) : notSetValue;
};

/*
     OM ERVOOR TE ZORGEN DAT DE OPERATIES NIET COMMUTATIEF ZIJN:
       bv een "rename" operatie invoeren, die een item hernoemt
       (en de inventory houdt bv alle namen bij om het makelijk te houden)
 */

// `GroceryList` is a list of grocery items,
// implemented as a SECRO (Strong Eventually Consistent Replicated Object).
// Notice that the `GroceryList` forms a CmRDT as all operations commute.
class GroceryList {
    constructor(items = []) {
        this.items = new Map(); // DCT mapping items to an object containing the requested and bought quantities.
        items.forEach(this.add.bind(this));
    }

    // Adds the requested quantity to the item.
    add(item) {
        const name = item.name;
        const quantities = this.items.getOrElse(name, {requested: 0, bought: 0});

        quantities.requested += item.requested;
        quantities.bought    += item.bought;

        this.items.set(name, quantities);
    }

    // We get informed that someone bought
    // a certain quantity of the given item.
    bought(itemName, quantity) {
        if (!this.items.has(itemName))
            throw new Error('Unknown item.');

        const quantities = this.items.get(itemName); // requested & bought quantities
        quantities.bought += quantity; // `quantities` is a reference, not a copy
    }

    // Accepts an optional parameter which it the name of an item.
    // If the optional parameter is provided it returns only that item.
    // Otherwise, an array of all items is returned.
    @accessor
    get(itemName) {
        if (itemName) {
            const quantities = this.items.getOrElse(itemName, {requested: 0, bought: 0});
            return new GroceryItem(itemName, quantities.requested, quantities.bought);
        }
        else {
            const items = [];
            this.items.forEach((quantities, name) => {
                const {requested, bought} = quantities;
                items.push(new GroceryItem(name, requested, bought));
            });
            return items;
        }
    }

    @accessor
    prettyPrint() {
        return this.get().reduce((str, item, idx) => {
            if (idx > 0) str += "\n";
            str += item.name + "(" + item.bought + "/" + item.requested + ")";
        }, "");
    }

    tojson() {
        return this.get();
    }

    static fromjson(items) {
        return new GroceryList(items);
    }
}

Factory.registerCmRDTClass(GroceryList);

module.exports = GroceryList;