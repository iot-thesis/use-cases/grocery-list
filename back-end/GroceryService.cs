//const GroceryItem = require('./Grocery');
const GroceryList = require('./GroceryList');
const Inventory   = require('./Inventory');

/*
 *   GroceryList contains GroceryItems  (GroceryList is a Secro)
 *      - add(item: GroceryItem)     --> if map.has(item.name) then add quantities else new entry
 *      - [ buy(itemName)            --> Buys the item (willen we niet laten propageren) ]
 *         Implement buy as a method of the Service --> because it coordinates between both, the inventory and the list
 *      - bought(itemName, quantity) --> Updates the quantity of that item in the list: item.quantity -= quantityBought
 *      - get(): GroceryItem[]       --> Returns an array of grocery items
 *
 *   Inventory of bought GroceryItems (maps GroceryItems to amount bought)   (Strongly consistent replica)
 *      - amountInStock(itemName: string): number --> returned hoeveel van dat item men reeds gekocht heeft
 *      - approve(itemName: string, stockQuantity: number, buyingQuantity: number): boolean
 *          --> The client asks the server if he can buy a `buyingQuantity` quantity of `itemName` items.
 *              The client is aware/thinks there are `stockQuantity` of this item in stock
 *              (i.e. is the amount the server responded with when asking `amountInStock`).
 *              If the server approves this means he trusts you will buy that quantity and he adds the quantity to the inventory.
 */

deftype Grocery

service GroceryService {
    rep groceryList = new GroceryList(); // Available replica
    rep inventory   = new Inventory();   // Consistent replica, maintaining the purchase inventory

    // Creator provides information about the grocery list
    // This information should never change as it is not kept consistent in any way.
    constructor(name, author) {
        this.name = name;
        this.author = author;
    }

    // Buying an item requires coordination
    // between the inventory and the grocery list.
    buy(itemName, buyingQuantity) {
        return new Promise((resolve, reject) => {
            if (buyingQuantity <= 0) {
                console.log('Rejected due to <= 0 quantity');
                reject("Buy request (" + buyingQuantity + " x " + itemName + ") rejected by the inventory. Quantity must at least be 1.");
            }

            // Amount of pieces that we know have already been bought of this item
            const stockQuantity = this.groceryList.get(itemName).bought;

            // Send buy request to the inventory.
            // Includes the amount we believe has already been bought.
            // Such that the inventory can reject our request if the amount is not correct (e.g. due to a concurrent buy).
            this.inventory
                .then(inventory => {
                    return inventory.approve(itemName, stockQuantity, buyingQuantity)
                })
                .then(accepted => {
                    if (accepted) {
                        // Inform grocery list that we succeeded buying the given quantity
                        this.groceryList.bought(itemName, buyingQuantity);
                        resolve();
                    }
                    else {
                        reject("Buy request (" + buyingQuantity + " x " + itemName + ") rejected by the inventory. Please try again.");
                    }
                });
        });
    }
}

function createGroceryService(name, author, updateCb) {
    const service = new GroceryService(name, author);
    // Register an onUpdate listener which pushes updates to the front-end
    service.groceryList.onUpdate(updateCb);
    publish service as Grocery;
    return service;
}

function sub(callback) {
    subscribe Grocery with callback;
}

module.exports.createGroceryService = createGroceryService;
module.exports.sub = sub;