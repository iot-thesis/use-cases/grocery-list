/*
 * `GroceryItem` class representing grocery items.
 * A grocery item contains a name and quantity.
 * The name of a grocery item identifies the item in the grocery list.
 */

class GroceryItem {
    constructor(name, quantity, bought = 0) {
        this.name      = name;
        this.requested = quantity;
        this.bought    = bought;
    }

    tojson() {
        return {name: this.name, requested: this.requested, bought: this.bought};
    }

    static fromjson(item) {
        return new GroceryItem(item.name, item.requested, item.bought);
    }
}

Factory.registerExchangeableClass(GroceryItem);

module.exports = GroceryItem;