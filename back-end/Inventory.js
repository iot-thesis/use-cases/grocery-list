
/*
 *   GroceryList contains GroceryItems  (GroceryList is a Secro)
 *      - add(item: GroceryItem)     --> if map.has(item.name) then add quantities else new entry
 *      - [ buy(itemName)            --> Buys the item (willen we niet laten propageren) ]
 *         Implement buy as a method of the Service --> because it coordinates between both, the inventory and the list
 *      - bought(itemName, quantity) --> Updates the quantity of that item in the list: item.quantity -= quantityBought
 *      - get(): GroceryItem[]       --> Returns an array of grocery items
 *
 *   Inventory of bought GroceryItems (maps GroceryItems to amount bought)   (Strongly consistent replica)
 *      - amountInStock(itemName: string): number --> returned hoeveel van dat item men reeds gekocht heeft
 *      - approve(itemName: string, stockQuantity: number, buyingQuantity: number): boolean
 *          --> The client asks the server if he can buy a `buyingQuantity` quantity of `itemName` items.
 *              The client is aware/thinks there are `stockQuantity` pieces of this item in stock
 *              (i.e. is the amount the server responded with when asking `amountInStock`).
 *              If the server approves this means he trusts you will buy that quantity and he adds the quantity to the inventory.
 */

/*
 * `Inventory` class maintains an inventory of our purchases.
 * For each item it maintains the amount of pieces that were bought.
 * Before buying an item, user's should send a buy request to the inventory,
 * which can either be approved or rejected.
 */

class Inventory {
    constructor(stock = []) {
        this.stock = new Map(stock);
    }

    // Returns how many pieces of this item we already bought
    /*
    amountInStock(itemName) {
        return this.stock.getOrElse(itemName, 0);
    }
    */

    // Accepts or rejects the user's request.
    // The request contains the item's name,
    // the quantity the user believes was already bought
    // and the quantity the user wants to buy.
    approve(itemName, stockQuantity, buyingQuantity) {
        const trueStock = this.stock.getOrElse(itemName, 0);

        if (trueStock !== stockQuantity) {
            // Reject, since the true stock does not correspond to the user's belief
            return false;
        }
        else {
            // Accept
            // The user will buy the requested quantity,
            // therefore, add it to the inventory.
            this.stock.set(itemName, trueStock + buyingQuantity);
            return true;
        }
    }

    /*
    // Only possible if no pieces of that item were bought yet.
    delete(item) {

    }
    */
}

module.exports = Inventory;